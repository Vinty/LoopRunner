using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player;

public class CameraController : MonoBehaviour {
	private Player.Player player;

	public void RegisterPlayer(Player.Player playerForRegister) {
		player = playerForRegister;
	}

	private void Update() {
		if(player == null && player.currentStage != PlayerStage.MOVE) {
			return;
		}
		
		transform.SetParent(player.transform);
		transform.localPosition    = new Vector3(1f, -15, 11);
		transform.localEulerAngles = new Vector3(-60f, -180f, 180f);
	}
}
