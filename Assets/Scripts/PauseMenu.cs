using System;
using UnityEngine;

public class PauseMenu : MonoBehaviour {
	public static bool       GameIsPaused = false;
	public        GameObject pauseMenuUI;

	[SerializeField] private GameManager gameManager;

	private void Awake() {
		gameManager = FindObjectOfType<GameManager>();
	}

	void Update() {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			if(GameIsPaused) {
				Resume();
			}
			else {
				Pause();
			}
		}
	}

	public void Pause() {
		pauseMenuUI.SetActive(true);
		Time.timeScale = 0;
		GameIsPaused   = true;
	}

	public void Resume() {
		pauseMenuUI.SetActive(false);
		Time.timeScale = 1f;
		GameIsPaused   = false;
	}

	public void SaveGame() {
		Debug.Log("Save Game...");
		gameManager.SaveGame();
	}

	public void LoadGame() {
		Debug.Log("Loading last level...");
		gameManager.LoadGame();
	}
}
