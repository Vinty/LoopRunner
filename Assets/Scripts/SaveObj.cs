using System.IO;
using UnityEngine;

public class SaveObj : MonoBehaviour {
	public static void SaveFileJson(object obj) {
		File.WriteAllText(GameManager.path, JsonUtility.ToJson(obj));
	}
}
