using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum Path {
	HORIZON_RIGHT,
	VERTICAL_DOWN,
	HORIZON_LEFT,
	VERTICAL_UP
}

[Serializable]
public class GroundManager : MonoBehaviour {
	[SerializeField] private GameObject groundPrefab;
	[SerializeField] private Material   startPanelMaterial;

	private readonly List<GameObject> resultArray = new List<GameObject>();
	private          GameObject       currentGroundPanel;
	private          Vector3          currGroundPanelPosition       = Vector3.zero;
	public event Action<Vector3>      NextGroundPanelPositionAction = delegate {};
	private int                       currentCountPrefab            = 0;

	public int        CurrentIndexArray {get; set;}
	public bool       IsPanelsReady     {set; get;}
	public GameObject StartGroundPanel  {get; set;}

	private readonly Vector2Int[] anchors = {
		new Vector2Int(0, 0), new Vector2Int(15, 0), new Vector2Int(15, -15), new Vector2Int(0, -15)
	};

	public void CreateGround() {
		StageOneCreateTop(anchors[1], anchors[2]);
		StageTwoCreateRight(anchors[2], anchors[3]);
		StageThreeCreateDown(anchors[3], anchors[0]);
		StageForeCreateLeft(anchors[0]);
	}

	private void StageOneCreateTop(Vector2Int anchor, Vector2Int anchor2) {
		while(currGroundPanelPosition.x < anchor.x
		   && currGroundPanelPosition.y > anchor2.y) {
			createFullPanel(Path.HORIZON_RIGHT, Random.Range(3, 5));
			createFullPanel(Path.VERTICAL_DOWN, Random.Range(1, 3));
		}
	}

	private void StageTwoCreateRight(Vector2Int anchor, Vector2Int anchor2) {
		while(currGroundPanelPosition.x > anchor2.x
		   && currGroundPanelPosition.y > anchor.y) {
			createFullPanel(Path.VERTICAL_DOWN, Random.Range(2, 3));
			createFullPanel(Path.HORIZON_LEFT,  Random.Range(3, 7));
			createFullPanel(Path.VERTICAL_DOWN, Random.Range(3, 7));
		}
	}

	private void StageThreeCreateDown(Vector2Int anchor, Vector2Int anchor2) {
		while(currGroundPanelPosition.x > anchor.x
		   && currGroundPanelPosition.y < anchor2.y) {
			createFullPanel(Path.HORIZON_LEFT, Random.Range(2, 3));
			createFullPanel(Path.VERTICAL_UP,  Random.Range(3, 4));
			createFullPanel(Path.HORIZON_LEFT, Random.Range(3, 7));
		}
	}

	private void StageForeCreateLeft(Vector2Int anchor) {
		while(currGroundPanelPosition.x > anchor.x) {
			createFullPanel(Path.HORIZON_LEFT, 1);
		}

		while(currGroundPanelPosition.y < anchor.y) {
			createFullPanel(Path.VERTICAL_UP, 1);
		}

		while(currGroundPanelPosition.x < anchor.x) {
			createFullPanel(Path.HORIZON_RIGHT, 1);
		}
	}

	private void createFullPanel(Path pathFullPanel, int count) {
		for(int i = 0 ;
			i < count ;
			i++) {
			resultArray.Add(createGroundPanel(pathFullPanel));
		}
	}

	private GameObject createGroundPanel(Path path) {
		groundPrefab      = Instantiate(groundPrefab);
		groundPrefab.name = "Ground_Panel_" + currentCountPrefab++;
		groundPrefab.SetActive(false);

		if(currentGroundPanel) {
			groundPrefab.transform.position = GetNextPanelPosition(path);
			currentGroundPanel              = groundPrefab;
			currGroundPanelPosition         = currentGroundPanel.transform.position;
			return currentGroundPanel;
		}

		groundPrefab.transform.position = Vector3.zero;
		currentGroundPanel              = groundPrefab;
		currGroundPanelPosition         = currentGroundPanel.transform.position;
		return groundPrefab;
	}

	private Vector3 GetNextPanelPosition(Path currentPath) {
		Vector3 currentPosition = currentGroundPanel.transform.position;
		return currentPath switch {
			Path.HORIZON_RIGHT => new Vector3(currentPosition.x + 1, currentPosition.y),
			Path.VERTICAL_DOWN => new Vector3(currentPosition.x,     currentPosition.y - 1),
			Path.HORIZON_LEFT => new Vector3(currentPosition.x                         - 1, currentPosition.y),
			Path.VERTICAL_UP => new Vector3(currentPosition.x,                              currentPosition.y + 1),
		};
	}

	public IEnumerator ShowPanels() {
		foreach(GameObject panel in resultArray) {
			yield return new WaitForSeconds(0.015f);
			panel.SetActive(true);
		}

		IsPanelsReady = true;
		int randomNum = Random.Range(0, resultArray.Count);

		StartGroundPanel                                   = resultArray[randomNum];
		StartGroundPanel.GetComponent<Renderer>().material = startPanelMaterial;
		CurrentIndexArray                                  = randomNum;
	}

	public void SetNextPanelPosition() {
		if(CurrentIndexArray < resultArray.Count - 1) {
			NextGroundPanelPositionAction(resultArray[CurrentIndexArray + 1].transform.position);
			CurrentIndexArray++;
			currentGroundPanel = resultArray[CurrentIndexArray];
			return;
		}

		NextGroundPanelPositionAction(resultArray[0].transform.position);
		CurrentIndexArray  = 0;
		currentGroundPanel = resultArray[CurrentIndexArray];
	}
}
