using System;
using UnityEngine;

namespace Player {
	public enum PlayerStage {
		WAIT,
		MOVE,
		FIGHT,
		DIE
	}

	public class Player : MonoBehaviour {
		public                   PlayerStage currentStage = PlayerStage.WAIT;
		[SerializeField] private float       speedMove    = 0.5f;

		private GroundManager groundManager;
		private Vector3       nextMoveTarget = Vector3.zero;


		private void RegisterMeInCameraController() {
			CameraController cameraController = FindObjectOfType<CameraController>();
			cameraController.RegisterPlayer(this);
		}

		private void Awake() {
			groundManager                               =  FindObjectOfType<GroundManager>();
			groundManager.NextGroundPanelPositionAction += GetNextPanelPosition;
		}

		private void Start() {
			RegisterMeInCameraController();
		}

		private void Update() {
			switch(currentStage) {
				case PlayerStage.WAIT :
					break;
				case PlayerStage.MOVE :
					transform.eulerAngles = new Vector3(90f, 0, 0);
					float dist = Vector3.Distance(nextMoveTarget, transform.position);
					if(dist > 0.71f) {
						MoveTo();
						return;
					}

					groundManager.SetNextPanelPosition();
					break;
				case PlayerStage.FIGHT : 
					
					break;
				case PlayerStage.DIE :   break;
			}
		}

		private void GetNextPanelPosition(Vector3 pos) {
			nextMoveTarget = pos;
		}

		private void MoveTo() {
			if(nextMoveTarget.x > transform.position.x) {
				transform.Translate(Time.deltaTime * speedMove, 0, 0);
			}

			if(nextMoveTarget.x < transform.position.x) {
				transform.Translate(-Time.deltaTime * speedMove, 0, 0);
			}

			if(nextMoveTarget.y > transform.position.y) {
				transform.Translate(0, 0, -Time.deltaTime * speedMove);
			}

			if(nextMoveTarget.y < transform.position.y) {
				transform.Translate(0, 0, Time.deltaTime * speedMove);
			}
		}
	}
}
