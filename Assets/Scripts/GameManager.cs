using System;
using Player;
using UnityEngine;

public enum StageGame {
	PREPARE_CREATE_GROUND,
	BUILDING_GROUND,
	CREATE_PLAYER,
	CREATE_ENEMY,
	START,
	WAIT
}

[Serializable] // DTO Persist
public class GameManagerS {
	public float[] playerTransformPos;

	public bool     cameraMainIsEnableS;
	public bool     cameraIsometricIsEnableS;
	public StageGame currentStageS;
	public bool      isGameStartedFirstTimeS;
}

public class GameManager : MonoBehaviour, IPersist {
	[SerializeField] private GameObject playerObj;
	[SerializeField] private Camera     cameraMain;
	[SerializeField] private Camera     cameraIsometric;

	public static string path;

	private GroundManager groundManager;
	public  StageGame     currentStage           = StageGame.PREPARE_CREATE_GROUND;
	private bool          isGameStartedFirstTime = true;

	private void Start() {
		groundManager = FindObjectOfType<GroundManager>();
		CameraSwitch(cameraIsometric, false);
		path = System.IO.Path.Combine(Application.dataPath, "Save.json");
	}

	private void Update() {
		switch(currentStage) {
			case StageGame.PREPARE_CREATE_GROUND :
				if(!isGameStartedFirstTime) {
					currentStage = StageGame.START;
					return;
				}

				CameraSwitch(cameraMain, true);
				groundManager.CreateGround();
				StartCoroutine(groundManager.ShowPanels());
				currentStage = StageGame.BUILDING_GROUND;
				break;
			case StageGame.BUILDING_GROUND :
				if(!groundManager.IsPanelsReady) {
					return;
				}

				currentStage = StageGame.CREATE_PLAYER;
				break;
			case StageGame.CREATE_PLAYER :
				playerObj.name = "Player";
				Transform pTransform = groundManager.StartGroundPanel.transform;
				playerObj.transform.position    = new Vector3(pTransform.position.x, pTransform.position.y, -0.7f);
				playerObj.transform.eulerAngles = new Vector3(90f,                   0,                     0);
				playerObj.transform.localScale  = new Vector3(0.5f,                  0.5f,                  0.5f);

				isGameStartedFirstTime = false;
				currentStage           = StageGame.CREATE_ENEMY;
				break;
			case StageGame.CREATE_ENEMY :
				currentStage = StageGame.START;
				break;
			case StageGame.START :
				groundManager.SetNextPanelPosition();
				playerObj.GetComponent<Player.Player>().currentStage = PlayerStage.MOVE;
				currentStage                                         = StageGame.WAIT;
				CameraSwitch(cameraMain,      false);
				CameraSwitch(cameraIsometric, true);
				break;
			case StageGame.WAIT : break;
		}
	}

	private void CameraSwitch(Component cameraSwitch, bool onOff) {
		cameraSwitch.GetComponent<Camera>().enabled        = onOff;
		cameraSwitch.GetComponent<AudioListener>().enabled = onOff;
	}

	public void SaveObjJson() {
		var position = playerObj.transform.position;
		GameManagerS gameManagerS = new GameManagerS {
				playerTransformPos = new []{position.x, position.y, position.z}, 
				currentStageS           = currentStage,
				isGameStartedFirstTimeS = isGameStartedFirstTime,
			};
			SaveObj.SaveFileJson(gameManagerS);
	}

	public void LoadObjFromJson() {
		GameManagerS gameManagerS = LoadJson.LoadingJson<GameManagerS>();
		playerObj.transform.position = new Vector3(gameManagerS.playerTransformPos[0],
		                                           gameManagerS.playerTransformPos[1],
		                                           gameManagerS.playerTransformPos[2]
		);
		currentStage           = gameManagerS.currentStageS;
		isGameStartedFirstTime = gameManagerS.isGameStartedFirstTimeS;
	}

	public void SaveGame() {
		SaveObjJson();
	}

	public void LoadGame() {
		LoadObjFromJson();
	}
}
