using System;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour {
	[SerializeField] private GameObject canvasText;

	private int currentSecond;

	void Update() {
		int nowSecond = DateTime.Now.Second;
		if(currentSecond != nowSecond) {
			Clock(nowSecond);
		}
	}

	private void Clock(int nowSec) {
		string hourString    = CheckLength(DateTime.Now.Hour);
		string minutesString = CheckLength(DateTime.Now.Minute);
		string secondsString = CheckLength(nowSec);
		currentSecond = nowSec;

		canvasText.GetComponent<Text>().text = $"{hourString}:{minutesString}:{secondsString}";
	}

	private string CheckLength(int timeElement) {
		return timeElement.ToString().Length < 2 ? $"0{timeElement}" : timeElement.ToString();
	}
}
