using System.IO;
using UnityEngine;

public class LoadJson : MonoBehaviour {
	public static T LoadingJson<T>() {
		return File.Exists(GameManager.path) ?
			JsonUtility.FromJson<T>(File.ReadAllText(GameManager.path))
			: default(T);
	}
}
