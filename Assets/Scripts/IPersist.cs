public interface IPersist {
	
	void SaveObjJson();

	void LoadObjFromJson();

}
